import unittest
import logging.config
from pathlib import Path
from basicio import yaml as yam
from tests import resources
import yaml
rsc_path = resources.get_resource_path()

print('set up logger')
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


fluid01_props = {'density': 998, 'viscosity': 0.001, 'surf_tens_coef': 0.072}
fluid02_props = {'density': 1050, 'viscosity': 0.005, 'surf_tens_coef': 0.072}
test_data = {'fluid': fluid02_props}
test_yaml01 = {'fluid01': fluid01_props}
test_yaml02 = {'fluid02': fluid02_props}
test_yaml_list = [test_yaml01, test_yaml02]

test_yaml_dir = Path(rsc_path, 'tmp_yaml')


class TestYAML(unittest.TestCase):

    def test_write_dict_to_yaml_file_success(self):
        result = yam.write_dict2yaml(file_path=Path(test_yaml_dir, 'tmp_write.yml'), write_dict=fluid01_props)
        self.assertTrue(result)

    def test_change_dict_values_success(self):
        param_category = 'fluid'
        result = yam.change_yaml_dict_values(file_path=Path(test_yaml_dir, 'tmp_readchange.yml'),
                                             dict_name=param_category, new_dict=fluid02_props)
        self.assertTrue(result)

    def test_change_key_value_success(self):
        param_category = 'fluid'
        result = yam.change_yaml_dict_value(file_path=Path(test_yaml_dir, 'tmp_readchange.yml'), dict_name=param_category,
                                            key_name='density', new_value=1050)
        self.assertTrue(result)

    def test_read_yaml_success(self):
        yaml_data = yam.read_yaml(Path(test_yaml_dir, 'tmp_readchange.yml'))
        self.assertEqual(yaml_data, test_data)

    def test_read_yaml_not_found(self):
        with self.assertRaises(FileNotFoundError):
            yam.read_yamls(Path(test_yaml_dir, 'tmp_ready2change.yml'))

    def test_read_yaml_wrong_char(self):
        with self.assertRaises(yaml.YAMLError):
            yam.read_yaml(Path(test_yaml_dir, 'tmp_readerror.yml'))

    def test_read_file_of_yamls_success(self):
        yaml_data = yam.read_yamls(Path(test_yaml_dir, 'tmp_multipleYamls.yml'))
        self.assertEqual(yaml_data, test_yaml_list)

    def test_read_file_of_yamls_not_found(self):
        with self.assertRaises(FileNotFoundError):
            yam.read_yamls('tmp_multipleYamis.yml')

    def test_change_dict_values_wrong_dict(self):
        param_category = 'fluido'
        with self.assertRaises(KeyError):
            yam.change_yaml_dict_values(file_path=Path(test_yaml_dir, 'tmp_write.yml'),
                                        dict_name=param_category, new_dict=fluid02_props)


if __name__ == '__main__':
    unittest.main()
