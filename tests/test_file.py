import unittest
import logging.config
from pathlib import Path
from basicio import file
from tests import resources

rsc_path = resources.get_resource_path()

print('set up logger')
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

test_file_dir = Path(rsc_path, 'tmp_file')
write_dict = {'density': 998, 'viscosity': 0.001, 'surf_tens_coef': 0.072}
change_dict = {'density': 910, 'viscosity': 0.006, 'surf_tens_coef': 0.05}
config_dict = {'fluid': {'density': 998, 'viscosity': 0.001, 'surf_tens_coef': 0.072}}


class TestFile(unittest.TestCase):

    def test_write_dict_to_param_file(self):
        result = file.write_dict2file(Path(test_file_dir, 'tmp_writeDict'), write_dict)
        self.assertTrue(result)

    def test_write_comment_to_file(self):
        print('Test writing comment to file:')
        result = file.write_comment_line(Path(test_file_dir, 'tmp_writeComment'), 'this is a comment line')
        self.assertTrue(result)

    def test_write_config_to_param_file(self):
        result = file.write_dict2file(Path(test_file_dir, 'tmp_writeConfig'), param_dict=config_dict)
        self.assertTrue(result)

    def test_get_comment_line(self):
        result = file.get_section_item(title_str='This is a section item', comment_mark='//')
        print(result)
        result = file.get_subsection_item(title_str='This is a subsection item', comment_mark='//')
        print(result)

    def test_change_file_params(self):
        file.change_file_param_values(Path(test_file_dir, 'tmp_changeDict'), change_dict)


if __name__ == '__main__':
    unittest.main()
