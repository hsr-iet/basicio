import unittest
import os
import logging.config
from basicio import csv as basiccsv
from tests import resources

rsc_path = resources.get_resource_path()

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


class TestCSV(unittest.TestCase):
    test_output_path = '../build/test_output/'

    def setUp(self) -> None:
        os.makedirs(os.path.dirname(self.test_output_path), exist_ok=True)
        files = os.listdir(self.test_output_path)
        for file in files:
            os.remove(self.test_output_path + file)

    def test_write_xy_values2csv(self):
        filename = self.test_output_path + 'test.csv'
        basiccsv.write_xy2csv(filename, x_values=[1, 2, 3], y_values=[2, 4, 6])
        self.assertTrue(os.path.isfile(filename))

    def test_write_dict2csv(self):
        filename = self.test_output_path + 'test_dict2csv.csv'
        test_dict = dict(param01='value01', param02='value02', param03='value03')
        for i in range(0, 5):
            basiccsv.write_dict2csv(filename, test_dict)
        self.assertTrue(os.path.isfile(filename))


if __name__ == '__main__':
    unittest.main()
