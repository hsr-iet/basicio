import logging
import numpy as np  # type: ignore
import csv
from pathlib import Path
from typing import Optional, Dict, Union, List, Any
"""Basic csv manipulation commands.


author:         adrian rohner <adrian.rohner@ost.ch>

"""

logging.getLogger(__name__).addHandler(logging.NullHandler())
logger = logging.getLogger(__name__)


def _create_dict_from_csv(header: Any, values: Any) -> Dict[str, Any]:
    """Create a :py:class:`dict` from csv header and value input. Protected method - hence hidden in documentation.

    Parameters
    ----------
    header : :py:class:`numpy.ndarray`
        Name of the column.
    values : py:class:`numpy.ndarray`
        Value of the column.
    Returns
    -------
    :py:class:`dict`
        Dictionary of the csv columns.
    """

    csv_dict = {}

    for key, value in zip(header, values):
        csv_dict[str(key)] = value
    return csv_dict


def read_csv(*, file_path: str, header_line: Optional[List[str]] = None, delimiter: str = ',') -> \
        Dict[str, Union[str, float]]:
    """Read columns from a csv file, where delimiter and header line can be chosen by user.

    Parameters
    ----------
    file_path : :py:class:`str`
        Full path to the csv.
    header_line : :py:class:`list`, optional
        Header-line given by user if csv does not contain one.
    delimiter : :py:class:`str`, optional
        Delimiter character, default is ','.

    Returns
    -------
    :py:class:`dict`
        Dictionary of the csv columns.
    """

    if not header_line:
        header = np.genfromtxt('%s' % file_path, dtype='str', delimiter=',', max_rows=1, unpack=False)
        skipr = 1
    else:
        header = header_line
        skipr = 0

    values = np.loadtxt('%s' % file_path,  # file path
                        delimiter=delimiter,  # delimiter of columns
                        skiprows=skipr,  # number of rows to skip
                        unpack=True)  # transpose values, to get them column wise

    return _create_dict_from_csv(header, values)


def write_xy2csv(file_path: str, x_values: Any, y_values: Any) -> None:
    """Write csv file to folder.

    The given x- and y-lists are saved as columns.

    Parameters
    ----------
    file_path : :py:class:`str`
        Full path of the csv file.
    x_values : array_like
        X values of the data.
    y_values : array_like
        Y values of the data.

    Examples
    --------
    >>> x_list = [1, 2, 3]
    >>> y_list = [50, 100, 150]
    >>> write_xy2csv('test.csv', x_values=x_list, y_values=y_list)

    """
    rows = zip(x_values, y_values)

    with open('%s' % file_path, "w", newline='') as f:
        writer = csv.writer(f)
        for row in rows:
            writer.writerow(row)


def write_dict2csv(file_path: str, input_dict: Dict[str, Union[str, float]]) -> None:
    """Write dict to csv.

    If the file exists already, the header (dict keys) will not be written.

    Parameters
    ----------
    file_path : :py:class:`str`
        Full path of the csv file.
    input_dict : :py:class:`dict`
        Dictionary to write.

    """

    if Path(file_path).exists():
        is_header = True
    else:
        is_header = False

    with open(file_path, "a", newline='') as f:
        w = csv.DictWriter(f, input_dict.keys())
        if not is_header:
            w.writeheader()
        w.writerow(input_dict)
