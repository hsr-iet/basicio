#!/usr/bin/env python3

""" Index file for module BasicIO"""

# See https://docs.python.org/3/tutorial/modules.html#packages for more information

__author__ = "arohner"
__copyright__ = "Copyright (C) arohner"  # Check the copyrigth holder
__version__ = "0.2.0"
__email__ = "adrian.rohner@ost.ch"  # Check your email address
