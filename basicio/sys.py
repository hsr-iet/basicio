#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import shutil
import glob
from os import listdir
from os.path import basename, dirname, join
from typing import Optional, Union, List
from pathlib import Path

"""Basic system I/O, eg. save, copy, move folder and files etc.


author:         adrian rohner <adrian.rohner@hsr.ch>

"""

logging.getLogger(__name__).addHandler(logging.NullHandler())
logger = logging.getLogger(__name__)


def copy_folder(*, src_dir: str, dst_dir: str) -> bool:
    """Copy folder from source dir to destination dir.

    Parameters
    ----------
    src_dir : :obj:`str` 
        Source location.
    dst_dir : :obj:`str` 
        Destination location.

    Returns
    -------
    :obj:`bool` 
        True if the folder has been created.

    Raises
    ------
    FileExistsError
        Raises an error if the dst_dir already exists.
    FileNotFoundError
        Raises an error if the folder src_dir hasn't been found.
    """

    try:
        shutil.copytree(src_dir, dst_dir)
    except FileExistsError:
        logger.error('folder %s exists already - case not copied from template!' % dst_dir)
        raise
    except FileNotFoundError:
        logger.error('folder %s not found!' % src_dir)
        raise
    else:
        logger.info('folder %s created successfully!' % dst_dir)
        return True


def bkp_file(src_file_path: str, dst_file_dir: Optional[str] = None) -> None:
    """Create a backup file from a given input.

    Parameters
    ----------
    src_file_path : :obj:`str` 
        Source file full path.
    dst_file_dir : :obj:`str` 
        Destination location, optional, default = None, hence same place as source.

    Returns
    -------
    :obj:`bool` 
        True if the folder has been created.

    Raises
    ------
    FileExistsError
        Raises an error if the dst_dir already exists.
    FileNotFoundError
        Raises an error if the folder src_dir hasn't been found.
    """
    src_file_dir = dirname(src_file_path)

    if not dst_file_dir:
        dst_file_dir = src_file_dir
    file_name = basename(src_file_path)

    shutil.copyfile(src_file_path, join(dst_file_dir, 'bkp_%s' % file_name))


def copy_file(*, src_file_dir: str, dst_file_dir: str, file_ext: Optional[str] = None, file_name: Optional[str] = None,
              all_files: bool = False) -> None:
    """Copy files with the option of copying all files, or just some with specific file extension or file name.
       At least one of the three optional arguments has to be given

    Parameters
    ----------
    src_file_dir : :obj:`str` 
        Source file location.
    dst_file_dir : :obj:`str` 
        Destination file location.
    file_ext : :obj:`str` 
        file extension, optional, default = None.
    file_name : :obj:`str` 
        file name, optional, default = None.
    all_files : :obj:`bool` 
        flag for copying all files, optional, default = False.

    Raises
    ------
    IOError
        Raises an error if not all required input has been given.
    """
    try:
        if all_files:
            src_files = listdir(src_file_dir)
            for file_name in src_files:
                full_file_name = join(src_file_dir, file_name)
                shutil.copy2(full_file_name, dst_file_dir)
        elif file_ext:
            files = glob.iglob(join(src_file_dir, '*.' + file_ext))
            for file in files:
                shutil.copy2(file, dst_file_dir)
        elif file_name:
            shutil.copyfile(join(src_file_dir, file_name), join(dst_file_dir, file_name))
        else:
            shutil.copyfile(src_file_dir, dst_file_dir)
    except FileNotFoundError as err:
        logger.info('No files found!')
        logger.info(err)
        raise FileNotFoundError
    except IOError as err:
        logger.info('Error while copying files!')
        logger.info(err)
        raise IOError


def remove_folder(folder_dir: str) -> None:
    """ Remove a given folder.
    Parameters
    ----------
    folder_dir : :obj:`str` 
        Folder to remove.

    Raises
    ------

     IOError
        Any error raised during folder deletion.
    """
    try:
        shutil.rmtree(folder_dir)
    except FileNotFoundError as err:
        logger.info('Folder not found!')
        logger.info(err)
        raise FileNotFoundError
    except IOError as err:
        logger.info('Error while removing the folder!')
        logger.info(err)
        raise IOError


def get_files(search_path: Union[str, Path] = None, prefix: str = None, postfix: Union[list, str] = None,
              search_flag: str = None) -> List[Path]:
    """ Get files from path with specific pattern.

    Parameters
    ----------
    search_path : `:obj:str`, optional
        Search path, default is current folder.
    prefix : :py:class:`str`, optional
        Prefix search pattern.
    postfix : :py:class:`str`, optional
        Postfix search pattern.
    search_flag : :py:class:`str`, optional
        Random in-file search flag.
    as_list : :py:class:`bool`, optional
        Triggering the method to return a list of files instead of path-objects, default is `True`

    Returns
    -------
    `PosixPath`
        Returns a list of path objects.
    """
    files = []

    if not search_path:
        search_path = Path.cwd()
    else:
        if type(search_path) is str:
            search_path = Path(search_path)

    logger.info('search path is: %s' % search_path)
    if type(postfix) is list:
        glob_objs = []
        search_patterns = [f'*{ext}' for ext in postfix]
        for search_pattern in search_patterns:
            glob_objs.append(search_path.glob(search_pattern))

        for glob_obj in glob_objs:
            for file in glob_obj:
                files.append(file)
        return files

    else:
        search_pattern = '*'
        if prefix:
            search_pattern = f'{prefix}*'
        if postfix:
            search_pattern += f'{postfix}'
        if search_flag:
            search_pattern = f'*{search_flag}*'
        glob_obj = search_path.glob(search_pattern)

        for file in glob_obj:
            files.append(file)
        return files
