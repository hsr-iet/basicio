#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import yaml
from pathlib import Path
from os.path import basename
from typing import Dict, Any, Union, List

"""File Input/Output API using YAML Files.

See https://en.wikipedia.org/wiki/YAML for mor information on formatting yaml files.

author:         adrian rohner <adrian.rohner@ost.ch>

"""

logging.getLogger(__name__).addHandler(logging.NullHandler())
logger = logging.getLogger(__name__)


def write_dict2yaml(*, file_path: Union[str, Path], write_dict: Dict[Any, Any], mode: str = 'w') -> bool:
    """ Write :obj:`dict` to yaml file

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the yaml file.
    write_dict : :obj:`dict`
        Input Dictionary for writing.
    mode : :obj:`str`
        Write mode, default is to overwrite file.

    Returns
    -------
    :obj:`bool`
        True for successfully written and False for failed.

    Raises
    ------
    yaml.YAMLError
        If anything goes wrong while reading the yaml file.
    """

    try:
        with open('%s' % file_path, mode) as yaml_file:
            yaml.dump(write_dict, yaml_file, default_flow_style=False, sort_keys=False)
        logger.info('yaml file saved!')
        return True
    except yaml.YAMLError as err:
        logger.error('An error occurred while saving yaml file %s!' % basename(file_path))
        logger.error('Error: %s' % err)
        return False


def change_yaml_dict_values(*, file_path: Union[str, Path], dict_name: Dict[Any, Any], new_dict: Dict[Any, Any]) -> bool:
    """ Change 1st level dictionary in yaml file

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the yaml file.
    dict_name : :obj:`str`
        Name of the dictionary to change.
    new_dict : :obj:`dict`
        Dictionary with changed values.

    Returns
    -------
    :obj:`bool`
        True for successfully written and False for failed.

    Raises
    ------
    KeyError
        If the given value of `dict_name` couldn't be found.
    yaml.YAMLError
        If anything goes wrong while reading the yaml file.
    """

    yaml_data = read_yaml(file_path)
    yaml_entries = []
    for entry in yaml_data:
        yaml_entries.append(entry)
    if dict_name not in yaml_entries:
        raise KeyError
    yaml_data[dict_name] = new_dict
    try:
        with open('%s' % file_path, 'w') as f:
            yaml.dump(yaml_data, f, default_flow_style=False, sort_keys=False)
        return True
    except yaml.YAMLError as err:
        logger.error('An error occurred while changing yaml file!')
        logger.error('Error: %s' % err)
        return False


def change_yaml_dict_value(*, file_path: Union[str, Path], dict_name: str, key_name: str, new_value: Union[str, int]) -> bool:
    """ Change 2nd level, assuming it is a value and not another dictionary

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the yaml file.
    dict_name : :obj:`str`
        Name of the dictionary to change.
    key_name : :obj:`str`
        Name of the key.
    new_value : :obj:`str`, :obj:`int` 
        New values related to the key.

    Returns
    -------
    :obj:`bool`
        True for successfully written and False for failed.

    Raises
    ------
    KeyError
        If the given value of `dict_name` couldn't be found.
    yaml.YAMLError
        If anything goes wrong while reading the yaml file.
    """

    yaml_data = read_yaml(file_path)
    yaml_entries = []
    yaml_entry_keys = []
    for entry in yaml_data:
        yaml_entries.append(entry)
    if dict_name not in yaml_entries:
        raise KeyError
    for key in yaml_data[dict_name]:
        yaml_entry_keys.append(key)
    if key_name not in yaml_entry_keys:
        raise KeyError

    yaml_data[dict_name][key_name] = new_value

    try:
        with open('%s' % file_path, 'w') as f:
            yaml.dump(yaml_data, f, default_flow_style=False, sort_keys=False)
        return True
    except yaml.YAMLError as err:
        logger.error('An error occurred while changing yaml file!')
        logger.error('Error: %s' % err)
        return False


def read_yamls(file_path: Union[str, Path]) -> List[str]:
    """ Read multiple yaml docs from one file

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the yaml file.

    Returns
    -------
    yaml_list : :obj:`list`
        A list of all the yaml files within the document.

    Note
    -----
    Multiple yaml files in one document are separated by three dashed lines.
    See also: https://en.wikipedia.org/wiki/YAML

    Raises
    ------
    FileNotFoundError
        If the given file couldn't be found.
    yaml.YAMLError
        If anything goes wrong while reading the yaml file.

    """
    yaml_list = []
    try:
        with open(file_path, 'r') as stream:
            data_loaded = yaml.safe_load_all(stream)
            logger.info('file %s loaded' % stream.name)
            for doc in data_loaded:
                yaml_list.append(doc)
        return yaml_list
    except FileNotFoundError as err:
        logger.error('Yaml file %s could not be found!' % basename(file_path))
        logger.error('Error: %s' % err)
        raise
    except yaml.YAMLError as err:
        logger.error('An error occurred while reading yaml file %s!' % basename(file_path))
        logger.error('Error: %s' % err)
        raise


def read_yaml(file_path: Union[str, Path]) -> Dict[Any, Any]:
    """ Read a single yaml doc

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the yaml file.

    Returns
    -------
    yaml_dict : :obj:`dict`
        Returns a dictionary of the yaml file.

    Raises
    ------
    FileNotFoundError
        If the given file couldn't be found.
    yaml.YAMLError
        If anything goes wrong while reading the yaml file.

    """

    try:
        with open(file_path, 'r') as stream:
            try:
                yaml_dict = dict(yaml.safe_load(stream))
                logger.info('Yaml file %s loaded successfully!' % stream.name)
                return yaml_dict
            except yaml.YAMLError as err:
                logger.error('An error occurred while reading yaml file %s!' % basename(file_path))
                logger.error('Error %s%s' % (err.args[0], err.args[1]))
                raise
    except FileNotFoundError as err:
        logger.error('Yaml file %s could not be found!' % basename(file_path))
        logger.error('Error: %s' % err)
        raise
