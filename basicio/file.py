#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from pathlib import Path
from os.path import basename
from typing import Any, Dict, List, Union

"""Basic system I/O, eg. save, copy, move folder and files etc.


author:         adrian rohner <adrian.rohner@hsr.ch>

"""

logging.getLogger(__name__).addHandler(logging.NullHandler())
logger = logging.getLogger(__name__)

h1_sep = '='
h2_sep = '-'


def write_str2file(file_path: Union[str, Path], file_str: str, *, mode: str = 'w', newline=None) -> bool:
    """ Write string to file.

    Parameters
    ----------
    file_path : :obj:`str`
        Input file path.
    file_str : :obj:`str`
        Input string to write to file.
    mode: :obj:`str`
        Write mode, 'w' for write 'a' for append, default is write mode.

    Returns
    -------
    :obj:`bool` 
        True if successfully written the file.

    Raises
    ------
    IOError
        I/O error while writing file.

    """
    try:
        with open(file_path, mode, newline=newline) as stream:
            stream.write(file_str)
        logger.info('String written to file %s' % basename(file_path))
        return True
    except IOError as err:
        logger.error('Error while writing file: %s' % basename(file_path))
        logger.error('Error: %s' % err)
        raise


def change_file_param_values(file_path: Union[str, Path], param_dict: Dict[Any, Any], *, sep: str = ' ', end_mark: str = ';') -> None:
    """ Change values of an arbitrary input key-value pair file.

    This method will search through a given input file for the parameter keys and change the values
    of these given keys.

    Parameters
    ----------
    file_path : :obj:`str`
        Input file path.
    param_dict : :obj:`dict` 
        Input parameter dictionary.
    sep: :obj:`str`
        Separation string between parameter and value.
    end_mark : :obj:`str`
        End mark after value string.

    Raises
    ------
    IOError
        During file reading or file writing.
    FileNotFoundError
        If given file not found.

    """
    # new file lines
    file_lines = []

    # open the file where to change values
    try:
        with open('%s' % file_path, 'r') as inp_file:
            # go through all the lines in the file
            for line in inp_file:
                # set line changed to False
                ln_changed = False
                # go through the parameter list
                for key, value in param_dict.items():
                    # and check whether a given parameter is in the current line
                    if key in line:
                        logger.debug('line with parameter %s changed with value %s' % (key, value))
                        file_lines.append('{: <20}'.format(key) + sep + str(value) + end_mark + "\n")
                        ln_changed = True
                # if the line hasn't been changed, simply append it to the new file lines
                if not ln_changed:
                    file_lines.append(line)
    except FileNotFoundError as err:
        logger.error('File %s not found!' % basename(file_path))
        logger.error('Error: %s' % err)
        raise
    except IOError as err:
        logger.error('Error while reading file: %s' % basename(file_path))
        logger.error('Error: %s' % err)
        raise

    # write new values
    try:
        with open('%s' % file_path, 'w') as output:
            output.writelines(file_lines)
    except IOError as err:
        logger.error('Error while writing file: %s' % basename(file_path))
        logger.error('Error: %s' % err)


def write_dict2file(file_path: Union[str, Path], param_dict: Dict[Any, Any], *, mode: str = 'w', sep: str = ' ', end_mark: str = ';',
                    comment_mark: str = '//') -> bool:
    """ Write dictionary to parameter input file.

    Parameters
    ----------
    file_path : :obj:`str`
        Input file path.
    param_dict : :obj:`dict` 
        Input parameter dictionary.
    mode: :obj:`str`
        Write mode, 'w' for write 'a' for append, default is write mode.
    sep: :obj:`str`
        Separation string between parameter and value.
    end_mark : :obj:`str`,
        End mark after value string.
    comment_mark : :obj:`str`
        Comment mark for line which are commented, default is '//'.

    Returns
    -------
    :obj:`bool` 
        True if successfully written file.

    Raises
    ------
    IOError
        During file writing.


    """

    try:
        with open(file_path, mode) as stream:
            for key, val in param_dict.items():
                if type(val) is dict:
                    stream.write(comment_mark + ' {0}\n'.format(key))
                    for subkey, subval in val.items():
                        stream.write('{: <20}'.format(subkey) + sep + '{0}'.format(subval) + end_mark + '\n')
                else:
                    stream.write('{: <20}'.format(key) + sep + '{0}'.format(val) + end_mark + '\n')
        logger.info('values written to file: %s' % basename(file_path))
        return True
    except IOError as err:
        logger.error('Error while writing file: %s' % basename(file_path))
        logger.error('Error: %s' % err)
        raise


def write_line2file(*, file_path: Union[str, Path], line_str: str, mode: str = 'a') -> bool:
    """ Write single line.

    Parameters
    ----------
    file_path : :obj:`str`
        Input file path.
    line_str : :obj:`str`
        String to write.
    mode: :obj:`str`
        Write mode, 'w' for write 'a' for append, default is write mode.


    Raises
    ------
    IOError
        During file writing.
    """

    try:
        with open(file_path, mode) as stream:
            stream.write('{0}\n'.format(line_str))
        return True
    except IOError as err:
        logger.error('Error while writing file: %s' % basename(file_path))
        logger.error('Error: %s' % err)
        raise


def write_comment_line(file_path: Union[str, Path], comment_str: str, *, comment_mark: str = '#') -> bool:
    """ Write comment line.

    Parameters
    ----------
    file_path : :obj:`str`
        Input file path.
    comment_str : :obj:`str`
        String to write.
    comment_mark: :obj:`str`
        Write mode, 'w' for write 'a' for append, default is write mode.

    Raises
    ------
    IOError
        During file writing.

    """
    try:
        with open(file_path, 'a') as stream:
            stream.write(comment_mark + ' {0}\n'.format(comment_str))
        return True
    except IOError as exc:
        print(exc)
        return False


def __set_sep(line_length: int, sep: str) -> str:
    sep_line = ''
    for i in range(0, line_length - 4):
        sep_line += sep

    return sep_line


def get_section_item(*, title_str: str, comment_mark: str = '#') -> str:
    """ Write comment line.

    Parameters
    ----------
    title_str : :obj:`str`
        Given string for title.
    comment_mark: :obj:`str`
        Comment mark, like `//, #, etc.`, default is `#`.

    Returns
    -------
     title_line : :obj:`str`
        Returns string of section with given title.
    """
    title_line = comment_mark + ' {0}\n'.format(title_str)
    title_line += comment_mark + ' ' + __set_sep(len(title_line), h1_sep)

    return title_line


def get_subsection_item(*, title_str: str, comment_mark: str = '#') -> str:
    """ Getter for subsection

    Parameters
    ----------
    title_str : :obj:`str`
        Given string for title.
    comment_mark: :obj:`str`
        Comment mark, like `//, #, etc.`, default is `#`.

     Returns
     -------
     title_line : :obj:`str`
        Returns string of subsection with given title.
     """
    title_line = comment_mark + ' {0}\n'.format(title_str)
    title_line += comment_mark + ' ' + __set_sep(len(title_line), h2_sep)

    return title_line


def get_data_from_input_file(file_path: Union[str, Path]) -> Dict[str, Any]:
    """Get data from input file

    Parameters
    ----------
    file_path : :obj:`str`
        Full path of the input file.

    Returns
    -------
    param_dict : :obj:`dict` 
        dictionary of the input file key and values.

    Note
    ----
    With a format like: \n
    `#key` \n
    `value` \n
    and comment lines starting with `*`. \n
    Example: \n
    `* USD ClampOn & SETUP INPUT?` \n
    `******************************************************************************************` \n
    `#NumberOfDevices 	        (not used in current macro)` \n
    `1` \n
    `#PathProfile (I,V,W)        (Sensor Design, chose between "I" and "V")` \n
    `I` \n
    `#dzList in m                (Distance from Disturbance to Upstream Sensor` \n
    `5 10 15 30 60 80 100` \n
    """

    param_dict: Dict[str, Any] = {}

    # get the whole content of the file as list of lines
    with open(file_path) as f:
        file_content_raw = f.readlines()

    # remove line break (strip) and comment in line (split)
    a: List[List[str]] = []
    for line in file_content_raw:
        a.append(line.strip().split())
    file_content = a

    for i in range(0, len(file_content)):
        # if not last line
        if file_content[i]:
            # if the line contains a key value
            if '#' in file_content[i][0]:
                key = file_content[i][0].split('#')[1]
                value = file_content[i + 1]
                # if no value has been given at all
                if not value:
                    logger.info('adding key: %s with value: ''' % key)
                    param_dict[key] = ''
                else:
                    logger.info('adding key: %s with value: %s' % (key, value))
                    # checking if value is not a list
                    if len(value) < 2:
                        param_dict[key] = value[0]
                    else:
                        param_dict[key] = value
                i += 1

    return param_dict
