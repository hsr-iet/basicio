import os


def publish() -> None:
    os.system('semantic-release publish')


def version() -> None:
    os.system('semantic-release version --noop')


def changelog() -> None:
    os.system('semantic-release changelog --noop -v DEBUG')


def test() -> None:
    os.system('python -m unittest discover -s tests/ -v')


def mypy() -> None:
    os.system('mypy -p basicio')
