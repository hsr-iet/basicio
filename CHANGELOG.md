# Changelog

<!--next-version-placeholder-->

## v0.2.0 (2021-12-03)
### Feature
* **get_files:** New method to glob for files and folders ([`1c89839`](https://gitlab.com/hsr-iet/basicio/-/commit/1c89839c0d8ddcca72b9b79145f90e7c6e502884))
* Mypy implementation ([`c0be7be`](https://gitlab.com/hsr-iet/basicio/-/commit/c0be7be743d167f110f5f87774f4c5a3e509f06f))

### Fix
* Update lock file ([`548ea56`](https://gitlab.com/hsr-iet/basicio/-/commit/548ea56d07d9e99b16d245f10886f292dd0e303a))
* Allow newline str input in `write_str2file` ([`4bf5f68`](https://gitlab.com/hsr-iet/basicio/-/commit/4bf5f687aedaec2c13c92c3493c241a1ed35cb9f))
* Allow also Path objects as input ([`dd599c2`](https://gitlab.com/hsr-iet/basicio/-/commit/dd599c2dce6a1bab7356078344281dfb8d16d4f2))
* Allow also Path objects in write dicts ([`b01cf9a`](https://gitlab.com/hsr-iet/basicio/-/commit/b01cf9a30c6d28c5556cac34c15babab0d388c61))
* File copy also for full path ([`d777de9`](https://gitlab.com/hsr-iet/basicio/-/commit/d777de93b49b18ef1ecdf8006e188063b290b782))
* Change python version dep to 3.6 ([`ea5fcc7`](https://gitlab.com/hsr-iet/basicio/-/commit/ea5fcc747cd4260787a24a05c16b15d9f95e193a))
* Typhint error, change also docstring to array_like ([`c8cfca8`](https://gitlab.com/hsr-iet/basicio/-/commit/c8cfca844717e9f3d7653bd08b63d89150adc7c7))
* **xy2csv:** Change type hint to any for allowing all kinds of arrays ([`8bff6f9`](https://gitlab.com/hsr-iet/basicio/-/commit/8bff6f91b86584bb8883ca27e7d88ec90eaa6b75))
* Readme rename ([`3fa8b52`](https://gitlab.com/hsr-iet/basicio/-/commit/3fa8b523199ab328ebb3e51f6e868594d2508c3c))
* Removed README.rst ([`1b30ad3`](https://gitlab.com/hsr-iet/basicio/-/commit/1b30ad3a1ca6cb24601b6c0d1b9f682a8748164c))

### Documentation
* Change email ([`0fd80ce`](https://gitlab.com/hsr-iet/basicio/-/commit/0fd80ce56a9255f35be5e2692d74c6199a944c56))
