# BASICIO

## Setup

```shell
# install poetry
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
# check installation:
poetry --version

# update poetry
poetry self update
```

## Development Setup

```shell
poetry shell # creates virtual environment (on global system path)
poetry install

## alternative
poetry export -f requirements.txt > requirements.txt 
pip install -r requirements.txt

# check type hints
poetry run mypy
```

## Packaging

```shell
poetry build
poetry publish
```

